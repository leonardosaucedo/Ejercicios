var cadena = prompt("Ingrese una cadena:");

function recibirCadena(cadena) {
    var pi = cadena.lastIndexOf("(");
    var pec = cadena.indexOf(")");
    var salida = cadena.substring(pi, pec + 1);

    return salida;
}

function invertirCadena(cadenaRecibida) {
    var cadenaInvertida = "";

    var cadenaValida = cadenaRecibida.substring(1, cadenaRecibida.length - 1);

    for (var i = cadenaValida.length; i >= 0; i--) {
        cadenaInvertida = cadenaInvertida + cadenaValida.charAt(i);
    }

    return cadenaInvertida;
}

console.log(cadena);

var posicion = cadena.indexOf("(");

while (posicion != -1) {

    var salida1 = recibirCadena(cadena);

    var salida2 = invertirCadena(salida1);

    var cadena = cadena.replace(salida1, salida2);

    console.log(cadena);

    posicion = cadena.indexOf("(");
}