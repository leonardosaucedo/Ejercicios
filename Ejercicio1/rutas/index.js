'use strict'

const express = require('express');
const PaginaCtrl = require('../controladores/pagina');
const api = express.Router();

api.post('/llamada', PaginaCtrl.postMensaje);

module.exports = api;